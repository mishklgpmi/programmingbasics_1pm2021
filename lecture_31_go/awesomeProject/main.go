package main

import (
	"fmt"
	"net/http"
	"syscall"
	"unsafe"
)

var mod = syscall.NewLazyDLL("dlltest.dll")
var hello = mod.NewProc("hello")
var sum = mod.NewProc("sum")

func mainHandler(w http.ResponseWriter, r *http.Request) {
	ret, _, _ := hello.Call(123, 25)
	fmt.Fprintf(w, "<a href='/about'>About page %d</a>", ret)
}

func aboutHandler(w http.ResponseWriter, r *http.Request) {
	var numbers [3]int32 = [3]int32{2,3,4}
	ret, _, _ := sum.Call(uintptr(unsafe.Pointer(&numbers)), 3)
	Name := "Misha"
	fmt.Fprintf(w, "<a href='/'>Main page %d</a>", ret)
	fmt.Fprintf(w, "<p>Name is %s</p>", Name)
}

func main() {
	http.HandleFunc("/", mainHandler)
	http.HandleFunc("/about", aboutHandler)

	http.ListenAndServe(":8080", nil)
}